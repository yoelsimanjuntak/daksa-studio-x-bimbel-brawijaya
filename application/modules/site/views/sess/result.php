<?php
$ruser = GetLoggedUser();
$rtest = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTREMARKS1, null)
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$repps = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTREMARKS1, 'EPPS')
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$rist = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->order_by(COL_SEQ)->get(TBL_IST_RESULT)
->result_array();

$rpauli = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTTYPE, 'PAULI')
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$rkecermatan = $this->db
->where(COL_IDSESSION, $sess[COL_UNIQ])
->where(COL_TESTTYPE, 'ACR')
->order_by(COL_TESTSEQ, 'asc')
->get(TBL_TSESSIONTEST)
->result_array();

$arrChartIST = array();
$arrChartIST_ = array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <a href="<?=site_url('site/user/dashboard')?>" class="btn btn-sm btn-primary"><i class="far fa-arrow-circle-left"></i>&nbsp;&nbsp;DASHBOARD</a>
        <div class="card card-default mt-2">
          <div class="card-header">
            <h5 class="card-title m-0 font-weight-bold">INFORMASI UMUM</h5>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped" style="max-width: 100%">
              <tbody>
                <tr>
                  <td style="width: 10px; white-space: nowrap">NAMA</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$sess[COL_FULLNAME]?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">EMAIL</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=$sess[COL_EMAIL]?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">MULAI</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=date('d-m-Y H:i:s', strtotime($sess[COL_SESSTIMESTART]))?></strong></td>
                </tr>
                <tr>
                  <td style="width: 10px; white-space: nowrap">SELESAI</td>
                  <td style="width: 10px; white-space: nowrap">:</td>
                  <td><strong><?=date('d-m-Y H:i:s', strtotime($sess[COL_SESSTIMEEND]))?></strong></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <?php
        if(!empty($rtest)) {
          ?>
          <div class="card card-default">
            <div class="card-body p-0">
              <table class="table table-hover" style="max-width: 100%">
                <thead>
                  <tr>
                    <th>NAMA TEST</th>
                    <th>MULAI</th>
                    <th>SELESAI</th>
                    <th class="text-center">NILAI / SKOR</th>
                    <?php
                    if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                      ?>
                      <th class="text-center">OPSI</th>
                      <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum = 0;
                  foreach($rtest as $t) {
                    $arrPG = array();
                    $valPG = 0;
                    $rmtest = $this->db->where(COL_UNIQ, $t[COL_IDTEST])->get(TBL_MTEST)->row_array();
                    if(!empty($rmtest) && !empty($rmtest[COL_TESTPASSINGGRADE])) {
                      $arrPG = json_decode($rmtest[COL_TESTPASSINGGRADE]);
                    }
                    foreach($arrPG as $pg) {
                      if($pg->Group==$t[COL_TESTNAME]) $valPG = $pg->PG;
                    }

                    $rgroup = $this->db
                    ->select('QuestGroup, sum(QuestScore) as QuestScore')
                    ->where(COL_IDTEST, $t[COL_UNIQ])
                    ->where('QuestGroup IS NOT NULL')
                    ->order_by(COL_UNIQ)
                    ->group_by(COL_QUESTGROUP)
                    ->get(TBL_TSESSIONSHEET)
                    ->result_array();

                    $rquest = $this->db
                    ->select_sum(COL_QUESTSCORE)
                    ->where(COL_IDTEST, $t[COL_UNIQ])
                    ->get(TBL_TSESSIONSHEET)
                    ->row_array();
                    $valscore = $rquest[COL_QUESTSCORE];
                    $txtscore = number_format($rquest[COL_QUESTSCORE]);
                    if ($t[COL_TESTREMARKS]=='WAKTU HABIS') {
                      $txtscore = '<span class="text-danger">'.$txtscore.'</span>';
                    } else if(isset($t[COL_TESTSCORE])) {
                      $txtscore = number_format($t[COL_TESTSCORE]);
                      $valscore = $t[COL_TESTSCORE];
                    }
                    ?>
                    <tr>
                      <td style="vertical-align: middle"><?=strtoupper($t[COL_TESTNAME]).($valPG!=0?($valscore>=$valPG?'&nbsp;<span class="badge badge-success">LULUS</span>':'&nbsp;<span class="badge badge-danger">TIDAK LULUS</span>'):'')?></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><?=date('H:i:s', strtotime($t[COL_TESTSTART]))?></td>
                      <td style="vertical-align: middle; width: 10px; white-space: nowrap" class="text-right"><?=date('H:i:s', strtotime($t[COL_TESTEND]))?></td>
                      <td style="vertical-align: middle; white-space: nowrap" class="text-center"><strong><?=isset($t[COL_TESTSCORE])?number_format($t[COL_TESTSCORE]):number_format($rquest[COL_QUESTSCORE])?></strong></td>
                      <?php
                      if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                        ?>
                        <td style="vertical-align: middle; width: 10px;  white-space: nowrap" class="text-center">
                          <a target="_blank" href="<?=site_url('site/sess/review/'.$t[COL_UNIQ])?>" class="btn btn-xs btn-success" title="Pembahasan"><i class="far fa-tasks"></i></a>&nbsp;
                          <a target="_blank" href="<?=site_url('site/sess/review-print/'.$t[COL_UNIQ])?>" class="btn btn-xs btn-primary" title="Pembahasan"><i class="far fa-print"></i></a>
                        </td>
                        <?php
                      }
                      ?>
                    </tr>
                    <?php
                    if(!empty($rgroup)) {
                      foreach($rgroup as $g) {
                        $valPG = 0;
                        foreach($arrPG as $pg) {
                          if($pg->Group==$g[COL_QUESTGROUP]) $valPG = $pg->PG;
                        }
                        ?>
                        <tr>
                          <td style="vertical-align: middle; padding-left: 2.5rem !important" class="font-italic" colspan="3"><?=strtoupper($g[COL_QUESTGROUP]).($valPG!=0?($g[COL_QUESTSCORE]>=$valPG?'&nbsp;<span class="badge badge-success float-right">LULUS</span>':'&nbsp;<span class="badge badge-danger float-right">TIDAK LULUS</span>'):'')?></td>
                          <td style="vertical-align: middle; white-space: nowrap" class="text-center"><?=number_format($g[COL_QUESTSCORE])?></td>
                          <?php
                          if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                            ?>
                            <td style="vertical-align: middle; width: 10px;  white-space: nowrap" class="text-center"></td>
                            <?php
                          }
                          ?>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                    <?php
                    //$sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:0;
                    //$sum += $valscore;
                    if(!(strpos(strtolower($t[COL_TESTNAME]), 'pass hand') !== false)) {
                      $sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:$rquest[COL_QUESTSCORE];
                    }
                  }
                  ?>
                  <tr>
                    <th colspan="3" class="text-right">TOTAL</th>
                    <th class="text-center"><?=number_format($sum)?></th>
                    <?php
                    if($ruser[COL_ROLEID]==ROLEADMIN || true) {
                      ?>
                      <th></th>
                      <?php
                    }
                    ?>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        }
        ?>

        <?php
        if(!empty($repps)) {
          ?>
          <div class="card card-default">
            <div class="card-body p-0">
              <table class="table table-bordered" style="max-width: 100%">
                <thead>
                  <tr>
                    <th colspan="4">NAMA TEST</th>
                    <th>MULAI</th>
                    <th>SELESAI</th>
                    <?php
                    if($ruser[COL_ROLEID]==ROLEADMIN) {
                      ?>
                      <th class="text-center">#</th>
                      <?php
                    }
                    ?>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum = 0;
                  foreach($repps as $t) {
                    $rquest = $this->db
                    ->select_sum(COL_QUESTSCORE)
                    ->where(COL_IDTEST, $t[COL_UNIQ])
                    ->get(TBL_TSESSIONSHEET)
                    ->row_array();
                    $txtscore = number_format($rquest[COL_QUESTSCORE]);
                    if ($t[COL_TESTREMARKS]=='WAKTU HABIS') {
                      $txtscore = '<span class="text-danger">'.$txtscore.'</span>';
                    } else if(isset($t[COL_TESTSCORE])) {
                      $txtscore = number_format($t[COL_TESTSCORE]);
                    }

                    $rskor = array();
                    $repps_ = $this->db
                    ->where(COL_IDSESSION, $t[COL_IDSESSION])
                    ->where(COL_IDTEST, $t[COL_UNIQ])
                    ->get(TBL_EPPS_SESSION)
                    ->row_array();

                    if(!empty($repps_)) {
                      $rskor = $this->db
                      ->where(COL_EPPSSESSID, $repps_[COL_UNIQ])
                      ->get(TBL_EPPS_SESSIONDET)
                      ->result_array();
                    }
                    ?>
                    <tr>
                      <td style="vertical-align: top" colspan="4"><?=strtoupper($t[COL_TESTNAME])?></td>
                      <td style="vertical-align: top; width: 10px; white-space: nowrap" class="text-right" <?=count($rskor)>0?'rowspan="'.(count($rskor)+2).'"':''?>><?=date('H:i:s', strtotime($t[COL_TESTSTART]))?></td>
                      <td style="vertical-align: top; width: 10px; white-space: nowrap" class="text-right" <?=count($rskor)>0?'rowspan="'.(count($rskor)+2).'"':''?>><?=date('H:i:s', strtotime($t[COL_TESTEND]))?></td>
                      <?php
                      if($ruser[COL_ROLEID]==ROLEADMIN) {
                        ?>
                        <td style="vertical-align: top; width: 10px;  white-space: nowrap" class="text-center" <?=count($rskor)>0?'rowspan="'.(count($rskor)+2).'"':''?>>
                          <a target="_blank" href="<?=site_url('site/sess/review/'.$t[COL_UNIQ])?>" class="btn btn-xs btn-block btn-success" title="Pembahasan"><i class="far fa-tasks"></i></a>
                        </td>
                        <?php
                      }
                      ?>
                    </tr>
                    <tr class="text-sm">
                      <th class="text-center">Faktor</th>
                      <th class="text-center">Skor</th>
                      <th class="text-center">%</th>
                      <th class="text-center">Kategori</th>
                    </tr>
                    <?php
                    foreach($rskor as $r) {
                      ?>
                      <tr class="text-sm">
                        <td class="text-center"><?=$r[COL_EPPSFAKTOR]?></td>
                        <td class="text-center"><?=number_format($r[COL_EPPSSCORE])?></td>
                        <td class="text-center"><?=number_format($r[COL_EPPSPERCENTILE])?></td>
                        <td class="text-center"><?=$r[COL_EPPSCATEGORY]?></td>
                      </tr>
                      <?php
                    }
                    //$sum += isset($t[COL_TESTSCORE])?$t[COL_TESTSCORE]:0;
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        }

        if(!empty($rist)) {
          ?>
          <div class="row">
            <div class="col-sm-6">
              <div class="card card-default">
                <div class="card-header">
                  <h5 class="card-title m-0 font-weight-bold">RINCIAN</h5>
                </div>
                <div class="card-body p-0">
                  <table class="table table-bordered table-md">
                    <thead>
                      <tr>
                        <th style="width: 10px; white-space: nowrap">KOMPONEN</th>
                        <th style="width: 100px; white-space: nowrap">RW</th>
                        <th style="width: 100px; white-space: nowrap">SW</th>
                        <th>KETERANGAN</th>
                        <?php
                        if($ruser[COL_ROLEID]==ROLEADMIN) {
                          ?>
                          <th>#</th>
                          <?php
                        }
                        ?>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      foreach($rist as $r) {
                        $rtest_ = $this->db
                          ->where(COL_IDSESSION, $sess[COL_UNIQ])
                          ->where(COL_TESTREMARKS1, $r[COL_KODE])
                          ->get(TBL_TSESSIONTEST)
                          ->row_array();
                        ?>
                        <tr <?=$r[COL_KODE]=='JML'||$r[COL_KODE]=='IQ'?'class="font-weight-bold"':''?>>
                          <th style="width: 10px; white-space: nowrap" class="text-center"><?=$r[COL_KODE]?></th>
                          <td style="width: 100px; white-space: nowrap" class="text-center" <?=$r[COL_KODE]!='IQ'?'':'colspan="2"'?>><?=$r[COL_RW]?></td>
                          <?php
                          if($r[COL_KODE]!='IQ') {
                            ?>
                            <td style="width: 100px; white-space: nowrap" class="text-center"><?=$r[COL_SW]?></td>
                            <?php
                          }
                          ?>
                          <td><?=$r[COL_KATEGORI]?></td>
                          <?php
                          if($ruser[COL_ROLEID]==ROLEADMIN) {
                            ?>
                            <td>
                              <?php
                              if(!empty($rtest_)) {
                                ?>
                                <a target="_blank" href="<?=site_url('site/sess/review/'.$rtest_[COL_UNIQ])?>" class="btn btn-xs btn-block btn-success" title="Pembahasan"><i class="far fa-tasks"></i></a>
                                <?php
                              } else {
                                ?>
                                -
                                <?php
                              }
                              ?>
                            </th>
                          </td>
                            <?php
                          }
                          ?>
                        <?php
                        if($r[COL_KODE]!='JML'&&$r[COL_KODE]!='IQ') {
                          $arrChartIST[] = $r[COL_SW];
                          $arrChartIST_[] = $r[COL_KODE];
                        }
                      }
                      ?>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="card card-default">
                <div class="card-header">
                  <h5 class="card-title m-0 font-weight-bold">GRAFIK</h5>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="chartIST" style="height:250px; min-height:250px"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
        }

        if(!empty($rpauli)) {
          foreach($rpauli as $rt) {
            ?>
            <div class="row">
              <div class="col-sm-6">
                <div class="card card-default">
                  <div class="card-header">
                    <h5 class="card-title m-0 font-weight-bold">RINCIAN <?=$rt[COL_TESTNAME]?></h5>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-bordered table-md">
                      <thead>
                        <tr>
                          <th style="width: 100px; white-space: nowrap">KOLOM</th>
                          <th style="width: 10px; white-space: nowrap">JLH. BENAR</th>
                          <th style="width: 10px; white-space: nowrap">JLH. SALAH</th>
                          <th style="width: 10px; white-space: nowrap">JLH. LEWAT</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $rkolom = $this->db
                        ->select('QuestText, count(*) as NumResponse, sum(QuestScore) as QuestScore, SUM(CASE WHEN QuestScore = 0 THEN 1 ELSE 0 END) AS QuestFalse')
                        ->where(COL_IDTEST, $rt[COL_UNIQ])
                        ->order_by(COL_UNIQ, 'asc')
                        ->group_by(COL_QUESTTEXT)
                        ->get(TBL_TSESSIONSHEET)
                        ->result_array();

                        $sumSkor = 0;
                        $sumFalse = 0;
                        $sumSkipped = 0;
                        $maxSkor = 0;
                        $minSkor = 999999999999999;
                        foreach($rkolom as $r) {
                          $wbar = 0;
                          if($r[COL_QUESTSCORE]>0) {
                            $wbar = ($r[COL_QUESTSCORE]/$rt[COL_TESTQUESTSUB])*100;
                          }
                          ?>
                          <tr>
                            <td><?=$r[COL_QUESTTEXT]?></td>
                            <td class="text-right"><?=number_format($r[COL_QUESTSCORE])?></td>
                            <td class="text-right"><?=number_format($r['QuestFalse'])?></td>
                            <td class="text-right"><?=number_format($rt[COL_TESTQUESTSUB]-$r['NumResponse'])?></td>
                          </tr>
                          <tr>
                            <td colspan="4" style="padding: 0 !important">
                              <div class="progress progress-xxs">
                                <div class="progress-bar-animated bg-info progress-bar-striped" role="progressbar" aria-valuenow="<?=$r[COL_QUESTSCORE]?>" aria-valuemin="0" aria-valuemax="<?=$rt[COL_TESTQUESTSUB]?>" style="width: <?=$wbar?>%">
                                  <span class="sr-only"><?=number_format($r[COL_QUESTSCORE])?></span>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <?php
                          $sumSkor += $r[COL_QUESTSCORE];
                          $sumFalse += $r['QuestFalse'];
                          $sumSkipped += ($rt[COL_TESTQUESTSUB]-$r['NumResponse']);

                          if($r[COL_QUESTSCORE]>$maxSkor) $maxSkor = $r[COL_QUESTSCORE];
                          if($r[COL_QUESTSCORE]<$minSkor) $minSkor = $r[COL_QUESTSCORE];
                        }
                        ?>
                      </tbody>
                      <thead>
                        <tr>
                          <th>TOTAL</th>
                          <th class="text-right"><?=number_format($sumSkor)?></th>
                          <th class="text-right"><?=number_format($sumFalse)?></th>
                          <th class="text-right"><?=number_format($sumSkipped)?></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="card card-default">
                  <div class="card-header">
                    <h5 class="card-title m-0 font-weight-bold">KESIMPULAN <?=$rt[COL_TESTNAME]?></h5>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-bordered table-md">
                      <tr>
                        <td style="width: 100px; white-space: nowrap">GARIS TIMBANG</td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">:</td>
                        <td class="text-right">(<strong><?=number_format($maxSkor)?></strong> + <strong><?=number_format($minSkor)?></strong>) / <strong>2</strong></td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">=</td>
                        <td class="text-right font-weight-bold"><?=number_format(($maxSkor+$minSkor)/2,2)?></td>
                      </tr>
                      <tr>
                        <td style="width: 100px; white-space: nowrap">KECEPATAN KERJA</td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">:</td>
                        <td class="text-right"><strong><?=number_format($sumSkor)?></strong> / <strong><?=number_format($rt[COL_TESTQUESTNUM])?></strong></td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">=</td>
                        <td class="text-right font-weight-bold"><?=number_format($sumSkor/$rt[COL_TESTQUESTNUM],2)?></td>
                      </tr>
                      <tr>
                        <td style="width: 100px; white-space: nowrap">KETELITIAN KERJA</td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">:</td>
                        <td class="text-right"><strong><?=number_format($sumFalse)?></strong> + <strong><?=number_format($sumSkipped)?></strong></td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">=</td>
                        <td class="text-right font-weight-bold"><?=number_format($sumFalse+$sumSkipped)?></td>
                      </tr>
                      <tr>
                        <td style="width: 100px; white-space: nowrap">KEAJEGAN KERJA</td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">:</td>
                        <td class="text-right"><strong><?=number_format($maxSkor)?></strong> - <strong><?=number_format($minSkor)?></strong></td>
                        <td style="width: 5px; white-space: nowrap; text-align: center">=</td>
                        <td class="text-right font-weight-bold"><?=number_format($maxSkor-$minSkor)?></td>
                      </tr>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
        }

        if(!empty($rkecermatan)) {
          foreach($rkecermatan as $rt) {
            ?>
            <div class="row">
              <div class="col-sm-6">
                <div class="card card-default">
                  <div class="card-header">
                    <h5 class="card-title m-0 font-weight-bold">RINCIAN <?=$rt[COL_TESTNAME]?></h5>
                  </div>
                  <div class="card-body p-0">
                    <table class="table table-bordered table-md">
                      <thead>
                        <tr>
                          <th style="width: 100px; white-space: nowrap">KOLOM</th>
                          <th style="width: 10px; white-space: nowrap">JLH. BENAR</th>
                          <th style="width: 10px; white-space: nowrap">JLH. SALAH</th>
                          <th style="width: 10px; white-space: nowrap">JLH. LEWAT</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $rkolom = $this->db
                        ->select('QuestText, count(*) as NumResponse, sum(QuestScore) as QuestScore, SUM(CASE WHEN QuestScore = 0 THEN 1 ELSE 0 END) AS QuestFalse')
                        ->where(COL_IDTEST, $rt[COL_UNIQ])
                        ->order_by(COL_UNIQ, 'asc')
                        ->group_by(COL_QUESTTEXT)
                        ->get(TBL_TSESSIONSHEET)
                        ->result_array();

                        $sumSkor = 0;
                        $sumFalse = 0;
                        $sumSkipped = 0;
                        $maxSkor = 0;
                        $minSkor = 999999999999999;
                        $noKolom = 1;
                        foreach($rkolom as $r) {
                          $wbar = 0;
                          if($r[COL_QUESTSCORE]>0) {
                            $wbar = ($r[COL_QUESTSCORE]/$rt[COL_TESTQUESTSUB])*100;
                          }
                          ?>
                          <tr>
                            <td><?='Kolom '.$noKolom?></td>
                            <td class="text-right"><?=number_format($r[COL_QUESTSCORE])?></td>
                            <td class="text-right"><?=number_format($r['QuestFalse'])?></td>
                            <td class="text-right"><?=number_format($rt[COL_TESTQUESTSUB]-$r['NumResponse'])?></td>
                          </tr>
                          <tr>
                            <td colspan="4" style="padding: 0 !important">
                              <div class="progress progress-xxs">
                                <div class="progress-bar-animated bg-info progress-bar-striped" role="progressbar" aria-valuenow="<?=$r[COL_QUESTSCORE]?>" aria-valuemin="0" aria-valuemax="<?=$rt[COL_TESTQUESTSUB]?>" style="width: <?=$wbar?>%">
                                  <span class="sr-only"><?=number_format($r[COL_QUESTSCORE])?></span>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <?php
                          $sumSkor += $r[COL_QUESTSCORE];
                          $sumFalse += $r['QuestFalse'];
                          $sumSkipped += ($rt[COL_TESTQUESTSUB]-$r['NumResponse']);

                          if($r[COL_QUESTSCORE]>$maxSkor) $maxSkor = $r[COL_QUESTSCORE];
                          if($r[COL_QUESTSCORE]<$minSkor) $minSkor = $r[COL_QUESTSCORE];
                          $noKolom++;
                        }
                        ?>
                      </tbody>
                      <thead>
                        <tr>
                          <th>TOTAL</th>
                          <th class="text-right"><?=number_format($sumSkor)?></th>
                          <th class="text-right"><?=number_format($sumFalse)?></th>
                          <th class="text-right"><?=number_format($sumSkipped)?></th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js/Chart.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  <?php
  if(!empty($rist)) {
    ?>
    var chartCanvasIST = $('#chartIST').get(0).getContext('2d');
    var chartIST = new Chart(chartCanvasIST, {
      type: 'line',
      data: {
        "datasets":[
          {
            "fill": false,
            "label":"SKOR",
            "tension":0,
            "animations":{"y":{"duration":2000,"delay":500}},
            "backgroundColor":"#6610f2",
            "data":<?=json_encode($arrChartIST)?>
          }
        ],
        "labels":<?=json_encode($arrChartIST_)?>
      },
      options: {
        responsive : true,
        scales: {
          yAxes: [{
            ticks: {
              min: 50,
              max: 160,
              stepSize: 10
            }
          }]
        }

      }
    });
    <?php
  }
  ?>
});
</script>
