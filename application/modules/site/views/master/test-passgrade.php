<?php
$currPG = array();
if(!empty($data[COL_TESTPASSINGGRADE])) {
  $currPG = json_decode($data[COL_TESTPASSINGGRADE]);
}
$rquest = $this->db
->select("IFNULL(mtestdetail.QuestGroup, mtest.TestName) as QuestGroup")
->join(TBL_MTEST,TBL_MTEST.'.'.COL_UNIQ." = ".TBL_MTESTDETAIL.".".COL_IDTEST)
->where(TBL_MTESTDETAIL.'.'.COL_IDTEST, $data[COL_UNIQ])
->group_by(TBL_MTESTDETAIL.'.'.COL_QUESTGROUP)
->get(TBL_MTESTDETAIL)
->result_array();
?>
<form id="form-package" action="<?=current_url()?>">
  <?php
  foreach($rquest as $q) {
    $val = 0;
    foreach($currPG as $pg) {
      if($pg->Group==$q[COL_QUESTGROUP]) $val = $pg->PG;
    }
    ?>
    <div class="form-group row">
      <label class="col-sm-4 control-label"><?=$q[COL_QUESTGROUP]?></label>
      <div class="col-sm-3">
        <input type="number" class="form-control text-right" name="PG_<?=$q[COL_QUESTGROUP]?>" value="<?=$val?>" required />
      </div>
    </div>
    <?php
  }
  ?>
</form>
<script type="text/javascript">
$(document).ready(function(){
  var form = $('#form-package');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  form.validate({
    submitHandler: function(form) {
      var modal = $(form).closest('modal');
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.innerHTML;
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
