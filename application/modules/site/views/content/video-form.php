<form id="form-doc" action="<?=current_url()?>" enctype="multipart/form-data">
  <div class="form-group">
    <label>Judul</label>
    <input type="text" class="form-control" name="<?=COL_CONTENTTITLE?>" value="<?=!empty($data)?$data[COL_CONTENTTITLE]:''?>" />
  </div>
  <div class="form-group">
    <label>HTML</label>
    <textarea class="form-control" rows="4" name="<?=COL_CONTENTDESC1?>"><?=!empty($data)?$data[COL_CONTENTDESC1]:''?></textarea>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  $("select", $('#form-doc')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('#form-doc').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var modal = $(form).closest('.modal');
      var btnSubmit = null;
      var txtSubmit = '';
      if(modal) {
        var btnSubmit = $('button[type=submit]', modal);
        var txtSubmit = btnSubmit.html();
        btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
        btnSubmit.attr('disabled', true);
      }

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            $('.btn-refresh-data').click();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
          $(form).closest('.modal').modal('hide');
        }
      });

      return false;
    }
  });
});
</script>
