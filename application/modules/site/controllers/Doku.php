<?php
class Doku extends MY_Controller {
  public function callback_payment($id=null) {
    $txtPayment = '';
    $rpayment = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPAYMENT)
    ->row_array();
    if(!empty($rpayment)) {
      $txtPayment = $rpayment[COL_PAYMENTNO];
    }

    $data['title'] = 'Pembayaran Selesai';
    $data['subtitle'] = 'Anda telah menyelesaikan transaksi <strong>'.$txtPayment.'</strong>. Silakan kembali ke dashboard akun anda untuk melanjutkan.';
    $this->load->view('site/doku/callback', $data);
  }

  public function callback_cancel($id=null) {
    $txtPayment = '';
    $rpayment = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TPAYMENT)
    ->row_array();
    if(!empty($rpayment)) {
      $txtPayment = $rpayment[COL_PAYMENTNO];
    }

    $data['title'] = 'Pembayaran Dibatalkan';
    $data['subtitle'] = 'Anda telah membatalkan transaksi. Silakan kembali ke dashboard akun anda untuk melanjutkan.';
    $this->load->view('site/doku/callback', $data);
  }

  public function notif_va() {
    $sapi_type = php_sapi_name();

    $notificationHeader = getallheaders();
    $notificationBody = file_get_contents('php://input');
    $notificationPath = '/site/doku/notif-va';
    $secretKey = DOKU_SECRETKEY;

    $digest = base64_encode(hash('sha256', $notificationBody, true));
    $rawSignature = "Client-Id:".$notificationHeader['Client-Id']."\n"
        ."Request-Id:".$notificationHeader['Request-Id']."\n"
        ."Request-Timestamp:".$notificationHeader['Request-Timestamp']."\n"
        ."Request-Target:".$notificationPath."\n"
        ."Digest:".$digest;

    $signature = base64_encode(hash_hmac('sha256', $rawSignature, $secretKey, true));
    $finalSignature = 'HMACSHA256='.$signature;

    if ($finalSignature == $notificationHeader['Signature']) {
      $resp = json_decode($notificationBody);
      $retval = 'OK';
      if(!empty($resp)) {
        $dtpayment = array();
        if(isset($resp->order) && isset($resp->order->invoice_number)) {
          if(isset($resp->channel) && isset($resp->channel->id)) $dtpayment[COL_PAYMENTVIA] = $resp->channel->id;
          if(isset($resp->transaction) && isset($resp->transaction->status)) {
            $dtpayment[COL_PAYMENTSTATUS] = $resp->transaction->status;
            if($resp->transaction->status == 'SUCCESS' && isset($resp->transaction->date)) {
              $dtpayment[COL_PAYMENTDATE] = date('Y-m-d H:i:s', strtotime($resp->transaction->date));
            }
          }

          if(!empty($dtpayment)) {
            $res = $this->db->where(COL_PAYMENTNO, $resp->order->invoice_number)->update(TBL_TPAYMENT, $dtpayment);
            if(!$res) {
              $retval = $err['message'];
            }

            if(isset($dtpayment[COL_PAYMENTSTATUS]) && $dtpayment[COL_PAYMENTSTATUS]=='SUCCESS') {
              $rpayment = $this->db
              ->where(COL_PAYMENTNO, $resp->order->invoice_number)
              ->get(TBL_TPAYMENT)
              ->row_array();

              if(!empty($rpayment)) {
                $rdet = $this->db
                ->where(COL_IDPAYMENT, $rpayment[COL_UNIQ])
                ->get(TBL_TPAYMENTDETAIL)
                ->result_array();

                foreach($rdet as $d) {
                  if($d[COL_TYPE]=='sess') {
                    $dat = array(
                      COL_IDPACKAGE=>$d[COL_IDREF],
                      COL_USERNAME=>$rpayment[COL_USERNAME],
                      COL_SESSREMARK1=>$d[COL_REMARKS1],
                      COL_SESSREMARK2=>$d[COL_REMARKS2],
                      COL_CREATEDBY=>$rpayment[COL_USERNAME],
                      COL_CREATEDON=>date('Y-m-d H:i:s')
                    );

                    $res = $this->db->insert(TBL_TSESSION, $dat);
                  }
                }
              }
            }
          }
        }
      }
      //return response($retval, 200)->header('Content-Type', 'text/plain');
      header('Content-Type: text/plain');
      if (substr($sapi_type, 0, 3) == 'cgi') header("Status: 200 ".$retval);
      else header("HTTP/1.1 200 ".$retval);
    } else {
      //return response('Invalid Signature', 500)->header('Content-Type', 'text/plain');
      header('Content-Type: text/plain');
      if (substr($sapi_type, 0, 3) == 'cgi') header("Status: 400 Invalid Signature");
      else header("HTTP/1.1 400 Invalid Signature");
    }
  }
}
?>
