<?php

class Home extends MY_Controller {

  public function index() {
    $this->load->model('mpost');

    $data['title'] = 'Beranda';
    $data['berita'] = $this->mpost->search(9,"",1);
    $data['program'] = $this->mpost->search(0,"",2);
    $this->load->view('site/home/index-new', $data);
  }

  public function page($slug) {
    $this->load->model('mpost');
    $data['title'] = 'Page';

    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTCATEGORYNAME];//strlen($rpost[COL_POSTTITLE]) > 50 ? substr($rpost[COL_POSTTITLE], 0, 50) . "..." : $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $data['berita'] = $this->mpost->search(5,"",1);
    $this->load->view('site/home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('adminlte' , 'home/_error', $data);
    } else {
      $this->template->load('adminlte' , 'home/_error', $data);
    }
  }

  public function testimoni() {
    $data['title'] = 'Testimoni';
    $this->load->view('site/home/testimoni', $data);
  }

  public function testimoni_add() {
    if(!empty($_POST)) {
      $this->db->trans_begin();
      try {
        $rec = array(
          COL_CONTENTTYPE=>'Testimonial',
          COL_CONTENTTITLE=>$this->input->post('TestimoniTitle'),
          COL_CONTENTDESC1=>$this->input->post('TestimoniName'),
          COL_CONTENTDESC2=>$this->input->post('TestimoniText'),
        );

        $res = $this->db->insert(TBL_WEBCONTENT, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('SELESAI');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    }
  }

  public function xendit_v1_inv_test() {
    // Response error : Array ( [error_code] => EMAIL_FORMAT_ERROR [message] => Customer email format is invalid )
    $options['secret_api_key'] = XENDIT_APIKEY;

    $xenditPHPClient = new XenditClient\XenditPHPClient($options);

    $external_id = 'chayra-001';
    $amount = 3000;
    $payer_email = 'rolassimanjuntak@gmail.com';
    $description = 'Testing';
    $opt = array(
      'locale' => 'id',
      'customer' => array(
        'given_names'=>'Rolas',
        'surname' => 'Doe',
        'email' => 'rolassimanjuntak@gmail.com',
        'mobile_number' => '+6287774441111'
      ),
      'items' => array(
        array('name'=>'Item 1', 'quantity'=>1, 'price'=>1000),
        array('name'=>'Item 2', 'quantity'=>1, 'price'=>2000)
      )
    );

    $response = $xenditPHPClient->createInvoice($external_id, $amount, $payer_email, $description, $opt);
    print_r($response);
  }

  public function xendit_v2_inv_test() {
     Xendit::setApiKey(XENDIT_APIKEY);
  }

  public function release_test_images() {
    $arrfiles = array_slice(scandir(MY_UPLOADPATH), 3);
    $arrdeleted = array();
    foreach($arrfiles as $f) {
      $rquest = $this->db->where(COL_QUESTIMAGE, $f)->get(TBL_MTESTDETAIL)->row_array();
      if(empty($rquest)) {
        unlink(MY_UPLOADPATH.$f);
        $arrdeleted[] = $f;
      }
    }

    echo count($arrdeleted);
  }

  public function package($kat=null) {
    if(IsLogin()) {
      redirect('site/user/package');
    }
    $data['title'] = 'Daftar Paket';
    $data['ogtitle'] = 'Daftar Paket';
    if(!empty($kat)) {
      $rkat = $this->db
      ->where(COL_UNIQ, $kat)
      ->get(TBL_MKATEGORI)
      ->row_array();
      if(!empty($rkat)) {
        $data['sub'] = $rkat[COL_KATEGORI];
        $data['ogtitle'] = 'Daftar Paket '.$rkat[COL_KATEGORI];
      }

      $this->db->where(COL_IDKATEGORI, $kat);
    }

    $data['rpkg'] = $this->db
    //->order_by(COL_PKGPRICE)
    ->where('(mtestpackage.PkgDate is null or (mtestpackage.PkgDate>=CURDATE()))')
    ->order_by(COL_PKGNAME)
    ->get(TBL_MTESTPACKAGE)
    ->result_array();

    if(!empty($_POST)) {
      if($this->input->post(COL_PASSWORD)!=$this->input->post('ConfirmPassword')) {
        ShowJsonError('Maaf, Konfirmasi Password yang anda masukkan tidak valid.');
        exit();
      }

      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_EMAIL))
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, alamat email '.$rexist[COL_EMAIL].' telah terdaftar. Silakan daftar menggunakan email yang belum pernah terdaftar sebelumnya.');
        exit();
      }

      $dat = array(
        COL_ROLEID=>ROLEUSER,
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$this->input->post(COL_EMAIL)
      );

      $res = $this->db->insert(TBL_USERS, $dat);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $ruser = $this->db
      ->where(COL_USERNAME,$dat[COL_USERNAME])
      ->get(TBL_USERS)
      ->row_array();

      $rpackage = $this->db
      ->where(COL_UNIQ, $this->input->post('IdPkg'))
      ->get(TBL_MTESTPACKAGE)
      ->row_array();

      $pkgUrl = '';
      if(!empty($rpackage)) {
        $remarks1 = '';
        $remarks2 = !empty($this->input->post(COL_SESSREMARK2))?$this->input->post(COL_SESSREMARK2):'';
        if(empty($this->input->post('IsRandom')) || $this->input->post('IsRandom') != 1) {
          $remarks1 = 'NORANDOM';
        }

        $pkgTxt = urlencode("Saya ingin mendapatkan akses simulasi *".strtoupper($rpackage[COL_PKGNAME])."* di ".$this->setting_web_desc."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
        //$pkgUrl = 'https://api.whatsapp.com/send?phone='.$this->setting_org_phone.'&text='.$pkgTxt;
        $pkgUrl = site_url('site/payment/create/sess/'.$rpackage[COL_UNIQ]).'?remarks1='.$remarks1.'&remarks2='.$remarks2;
      }

      SetLoginSession($ruser);
      ShowJsonSuccess('Pendaftaran berhasil. Anda akan dialihkan ke dashboard akun anda...', array(
        'redirect'=>site_url('site/user/dashboard'),
        'url'=>$pkgUrl
      ));
      exit();

    } else {
      $this->load->view('site/home/package', $data);
    }
  }
}
 ?>
